* [x] Feature 3
  * [x] Expense category
    * [x] name max 50 char
    * [x] owner foreign key to the user model, realted_name categories, cascade
    * [x] __str__ return self.name
  * [x] Account model
  * [x] receipt model
* [x] Feature 5
  * [x] create a list view for receipts
  * [x] create urls.py in receipts
  * [x] register this view in the receipts app with path "" and name home
  * [x] include receipts app in expense with "receipts/"
  * [x] follow template for list view
    * [x] figure out how to format datetimefield
* [x] Feature 6
  * [x] register the loginview in accout urls.py
  * [x] create a path in expenses that includes accounts urls with "accounts/"
  * [x] create templates and registration directories 
  * [x] create login.html
    * [x] post form
  * [x] in expenses' settings file set LOGIN_REDIRECT_URL="home"
* [x] Feature 8
  * [x] use requiredloginmixin as a parent class for the listview
  * [x] use queryset on the view to filter receipt objects where the purchaser is the user