from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory

# Register your models here.


class AccountAdmin(admin.ModelAdmin):
    pass


class ReceiptAdmin(admin.ModelAdmin):
    pass


class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Account, AccountAdmin)
admin.site.register(Receipt, ReceiptAdmin)
admin.site.register(ExpenseCategory, ExpenseCategoryAdmin)
